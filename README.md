## 调试部署+V: json59420
### 承接各类软件系统开发、毕设。（java、php、python自动化等等）
![输入图片说明](%E4%B8%AA%E4%BA%BA%E5%BE%AE%E4%BF%A1.png)
# 图书管理系统

## 说明
1、数据库在下面已发，若代码不懂需要付费指导或加内容，可加v：json59420，备注目的再加。

## 技术栈
- 数据库：MySQL
- 开发工具：IDEA
- 数据连接池：Druid
- Web容器：Apache Tomcat
- 项目管理工具：Maven
- 版本控制工具：Git
- 后端技术：Spring + SpringMVC + MyBatis（SSM）
- 前端框架：LayUI

## 介绍
基于SSM+Layui的图书管理系统

### 数据库

- 主要功能开发步骤见，本人博客，代码有不同的话，以gitee中的完整版为主， **数据库** 如下。


### 1、后台首页index（开发完毕）
![首页](https://images.gitee.com/uploads/images/2021/0405/151255_0818142c_8169242.png "首页截图.png")

### 2、类型管理 （开发完毕）
![类型管理](https://images.gitee.com/uploads/images/2021/0330/200057_8f13c065_8169242.png "类型管理.png")

### 3、图书信息管理 （开发完毕）
![图书信息管理](https://images.gitee.com/uploads/images/2021/0331/222943_f5a0a074_8169242.png "图书信息管理.png")

### 4、读者管理 （开发完毕）
![读者管理](https://images.gitee.com/uploads/images/2021/0401/175810_d5a6fbb7_8169242.png "读者管理.png")

### 5、公告管理（开发完毕）
![公告管理](https://images.gitee.com/uploads/images/2021/0401/225400_7d994428_8169242.png "公告管理.png")

### 6、管理员管理（开发完毕）
![管理员管理](https://images.gitee.com/uploads/images/2021/0402/120518_7bd5a495_8169242.png "管理员管理.png")

### 7、借阅管理（开发完毕）
![借阅管理](https://images.gitee.com/uploads/images/2021/0403/222828_78f16633_8169242.png "借阅管理.png")

### 8、统计分析（开发完毕）
![统计分析](https://images.gitee.com/uploads/images/2021/0405/151228_30afa91a_8169242.png "统计分析.png")

### 9、登录（开发完毕）
![登录](https://images.gitee.com/uploads/images/2021/0405/212740_f2ef2e47_8169242.png "登录.png")

### 10、退出登录和修改密码（开发完毕）
![退出登录与修改密码](https://images.gitee.com/uploads/images/2021/0406/122222_f99dd86e_8169242.png "退出登录与修改密码.png")

所有功能开发完毕



